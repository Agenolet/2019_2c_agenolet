/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*==================[inclusions]=============================================*/
#include "../inc/Proyecto3.h"
#include "../inc/Proyecto3.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "switch.h"
#include "timer.h"
#include "hc_sr4.h"
#include "DisplayITS_E0803.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/
#define NO_KEY 0
/*==================[internal data definition]===============================*/
bool in_cm = false; // 0  si en cm y 1 si en in (pulgadas es in)
bool on_off = true;
bool display = false;
bool timer_on_off = true;
/*==================[internal functions declaration]=========================*/
void Tecla1();
void Tecla2();
void Tecla3();
void Tecla4();

void TempInt(); //Se llama para activar la lectura

void ReadData();

serial_config puerto_serie; //Configuracion del puerto serie para comunicacion con pc

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
int main(void)
{
	/* Configuracion del puerto serie */
	puerto_serie.port = SERIAL_PORT_PC; //Que puerto se selecciona
	puerto_serie.baud_rate = 115200; //Baudios (no es lo mismo que bit rate, o por lo menos no siempre)
	//puerto_serie.pSerial = NO_INT; //Si no se pretende Recibir datos, se pone NO_INT
	puerto_serie.pSerial = ReadData; //Al recibir un dato, se ejecuta la interrupcion ReadData

	UartInit(&puerto_serie); //Init de UART con la config de arriba


	SystemClockInit(); //Init del Clock para
	LedsInit();
	SwitchesInit();
	HcSr04Init(T_FIL2,T_FIL3);

	gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};
	ITSE0803Init(pines);

	int16_t distancia = 0;

	//Config del temporizador
	timer_config temporizador;
	temporizador.period = 1000;
	temporizador.timer = TIMER_B;
	temporizador.pFunc = TempInt;
	TimerInit(&temporizador);
	TimerStart(TIMER_B);

	SwitchActivInt (SWITCH_1,Tecla1);
	SwitchActivInt (SWITCH_2,Tecla2);
	SwitchActivInt (SWITCH_3,Tecla3);
	SwitchActivInt (SWITCH_4,Tecla4);

    while(1)
    {
    	if(on_off == true)
    	{
    		if(in_cm == false)
    		{
    			distancia = HcSr04ReadDistanceCentimeters();
    			UartSendString(puerto_serie.port , UartItoa(distancia, 10)); //10 es la base en la que paso la distancia. En este caso es decimal
    			UartSendString(puerto_serie.port , " cm\r\n");
    		}

    		if(in_cm == true)
    		{
    			distancia = HcSr04ReadDistanceInches();
    			UartSendString(puerto_serie.port , UartItoa(distancia, 10));
    			UartSendString(puerto_serie.port , " in \r\n");
    			//UartSendBuffer(puerto_serie.port , UartItoa(distancia, 10) , 3); // el 3, me enviaria los bytes (8bits) en ascii, esto es porque no va a superar los 1000 el uint16
    		}

    		on_off = false;
    	}
      	if(display == false) //Refresca la lectura en el display
    		ITSE0803DisplayValue(distancia);
	}
	return 0;
}

void Tecla1() //Activa o desactiva el timer, por lo tanto tambien la medicion de distancia
{
	if (timer_on_off)
		TimerStop(TIMER_B);
	else
		TimerStart(TIMER_B);

	timer_on_off = !timer_on_off;
}
void Tecla2() //Detiene la muestra del valor en el display
{
	display = !display;
}
void Tecla3() //Cambia a cm
{
	in_cm = false;
}
void Tecla4() //Cambia a in
{
	in_cm = true;
}
void TempInt() //Modifica la bandera para poder ejecutar la lectura de la distancia
{
	on_off = true;
}
void ReadData() //Se ejecuta esto cuando se envia info desde la pc
{
	uint8_t data_read;
	UartReadByte(puerto_serie.port, &data_read);

	switch(data_read)
	{
	    case 'a':
	    	{
	    		if (timer_on_off)
	    			TimerStop(TIMER_B);
	    		else
	    			TimerStart(TIMER_B);
	    		timer_on_off = !timer_on_off;
	    	}
	    break;

	    case 's':
	    	display = !display;
	    break;

	    case 'd':
	    	in_cm = false;
	    break;

	    case 'f':
	    	in_cm = true;
	    break;
	}
}
/*==================[end of file]============================================*/
