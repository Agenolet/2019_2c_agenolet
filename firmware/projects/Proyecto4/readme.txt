﻿Proyecto 4

Se implementa un vúmetro, procesando la señal de audio obtenida con un micrófono. El procesamiento realizado es la aplicación de la transformada de Fourier
para trabajar con la frecuencia del sonido. El vúmetro consistirá en el cambio de la cantidad y color de un anillo de LEDs según las características del sonido. 
Teniendo en cuenta la frecuencia, el color del anillo de LEDs cambiará, siendo el verde para los graves (de 0Hz a 700Hz), el azul para los intermedios (de 700 Hz a 1,17kHz), el rojo para los agudos (de 1,17kHz a 1,6kHz) y blanco para los muy agudos (de 1,6kHz para arriba, sin límite superior). Teniendo en cuenta la intensidad, la cantidad de LEDs encendidos cambiará, siendo 0 para intensidad mínima y el máximo (12) para intensidad máxima.

Para implementar la aplicación, se requiere la conexión de dos dispositivos:

	Un micrófono
	Un NeoPixel

El micrófono se conecta:
	+V a 3V3
	GND a GNDA
	OUT a CH1 (P1,13)

El NeoPixel se conecta:
	+V a 5V
	GND a GND
	DATA OUT a GPIO1 (P2,32)