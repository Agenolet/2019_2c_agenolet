/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto4.h"
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "NeoPixel.h"
#include "switch.h" //No se utiliza, corroborar

//Defines e includes para poder aplicar fft
#define ARM_MATH_CM4
#define __FPU_PRESENT 1
#include "arm_math.h"
#include "arm_const_structs.h"

#define IFFT_FLAG 0
#define DO_BIT_REVERSE 1

//Ancho de ventana para aplicar fft
#define WINDOW_WIDTH 128

//Frecuencia de muestreo
#define SAMPLE_FREC_US (1000000/6000)


//#define LED_ON 1
//#define LED_OFF 0

//Booleano utilizado como bandera para comenzar una nueva ventana
bool new_window = false;

/*==================[macros and definitions]=================================*/

//Se define el GPIO1, ya que el driver de NeoPixel no es compatible con el enum del driver GPIO
#define GPIO1 1

/*==================[internal data definition]===============================*/

//Cantidad de datos a tomar
int N = 256;

//Vector que contiene la señal de audio
float32_t senial[2*WINDOW_WIDTH];

//Variable auxiliar que contiene el dato a guardar en el vector señal
uint16_t aux_senial;

//Contador asociado a la posición del vector señal
uint16_t contador_senial = 0;

/*==================[internal functions declaration]=========================*/

/** @brief Función llamada por timer.
 * Activa conversión AD.
 */
void TempInt();

/** @brief Función llamada por interrupción conversor AD.
 * Lee dato convertido
 */
void LecturaAD();

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();

	//Inicializamos unidad de punto flotante del uP
	fpuInit();

	//Variables usadas para la FFT
	uint8_t i, i_max = 0;
	float32_t fft[WINDOW_WIDTH], max = 0;

	//Fijamos configuración para inicializar conversor AD
	analog_input_config ad_config;
	ad_config.input = CH1;
	ad_config.mode = AINPUTS_SINGLE_READ;
	ad_config.pAnalogInput = LecturaAD;

	AnalogInputInit(&ad_config);
	AnalogOutputInit(); //Para medir en el ociloscipio la señal analogica

	//Inicialización anillo de LEDs
	NeoPixelInit(GPIO1, Anillo_Max2);
	//NeoPixelAllGreen(100);


	//Estructura para inicializar timer
	timer_config temporizador;
	temporizador.period = SAMPLE_FREC_US; //Período de muestreo
	temporizador.timer = TIMER_A; //Se usa SysTick
	temporizador.pFunc = TempInt;
	TimerInit(&temporizador);

	TimerStart(TIMER_A);

	//Valor de intensidad de encendido de LEDs
	uint8_t intensidad;

	//Cantidad de leds encendidos
	uint8_t cantidad_leds = 0;
	//Intensidad máxima fijada
	uint8_t intensidad_max = 80;

	//Valores RGB
	uint8_t R;
	uint8_t G;
	uint8_t B;

	//Contador para el for que determina intensidad
	int j;

	while(1)
	{
		if(new_window)
		{
			/* Procesamiento para realizar la FFT */
			arm_cfft_f32(&arm_cfft_sR_f32_len128, senial, IFFT_FLAG, DO_BIT_REVERSE);
			arm_cmplx_mag_f32(senial, fft, WINDOW_WIDTH);
			new_window = false;

			/* Obtención del valor maximo del espectro de frecuencias */
			for(i=2 ; i<WINDOW_WIDTH ; i++)
			{
				if(fft[i]>max)
				{
					//Intensidad
					max = fft[i];
					//Frecuencia a la que se encuentra la mayor energía
					i_max = i;
				}
			}

			intensidad = max;

			//Asigno un color a cada rango de frecuencia
			if (i_max < 15)
			{
				R = 0; G = 255; B = 0;
			}
			else if (i_max < 25)
			{
				R = 0; G = 0; B = 255;
			}
			else if (i_max < 35)
			{
				R = 255; G = 0; B = 0;
			}
			else if (i_max < 45)
			{
				R = 255; G = 255; B = 255;
			}

			//Defino cantidad de leds según el valor de intensidad
			for(j=1; j<=12; j++)
			{
				if (intensidad < ((intensidad_max/12)*j) && intensidad > ((intensidad_max/12)*(j-1)))
					cantidad_leds = j;
			}

			NeoPixelAllOFF();
			//Se prenden leds del NeoPixel, en una cantidad dependiente de la intensidad, y en un color dependiente de la frecuencia
			NeoPixelSetLen(cantidad_leds);
			NeoPixelAllSameColor(R, G, B, 1);

			max = 0;

			/* Prende distintos leds segun en que rango de frecuencias se encuentre el maximo */
			LedsOffAll();
			if (i_max < 15)
				LedOn(LED_3); // LED_3 ES VERDE
			else if (i_max < 25)
				LedOn(LED_2); // LED_3 ES ROJO
			else if (i_max < 35)
				LedOn(LED_1); // LED_1 ES AMARILLO
			else if (i_max < 45)
				LedOn(LED_RGB_B); //ESTA PRENDIENDO EL LED RGB, SOLO EL AZUL
			else
				LedsOffAll();
		}
	}
	return 0;
}

//Función del temporizador
void TempInt()
{
	//Función que comienza a convertir el dato AD, una vez que termina de convertir, llama a la función LecturaAD()
	AnalogStartConvertion();
}

void LecturaAD()
{
	//Lee dato y lo guarda en variable auxiliar
	AnalogInputRead(CH1, &aux_senial);
	AnalogOutputWrite(aux_senial);

	//Asigna el valor de la variable auxiliar al componente del vector correspondiente, separando en parte real e imaginaria
	if(contador_senial<WINDOW_WIDTH)
	{
		senial[contador_senial*2] = aux_senial*(3.3/1024); /* Parte real */
		senial[contador_senial*2+1] = 0; /* Parte imaginaria */

		//Aumenta el valor del contador en uno
		contador_senial++;
	}
	//Cuando el contador llega al máximo, se reinicia
	else
	{
		contador_senial = 0;
		new_window = true;
	}
}

/*==================[end of file]============================================*/
