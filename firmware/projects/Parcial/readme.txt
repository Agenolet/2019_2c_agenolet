﻿Parcial

Se implementa el firmware de un sistema de adquisicion de una señal de presion sanguinea y mostrara el maximo, minimo o promedio según uno lo desee.
El sistema controla constantemente el valor de presion maxima para indicar mediante el encendido de un led de "alarma" lo siguiente:
-Encendido de led Rojo si la misma supera 150mmHg
-Encendido de led Azul si la misma esta entre 50mmHg y 150mmHg
-Encendido de led Verde si la misma es inferior a 50mmHg 

Las teclas van a modificar lo que se desee mostrar en el display, con la iluminacion de un led para indicar qué es lo que se esta mostrando.
-Tecla 1: muestra el valor maximo de presion. Enciende un led rojo a modo indicativo.
-Tecla 2: muestra el valor minimo de presion. Enciende un led amarillo a modo indicativo.
-Tecla 3: muestra el valor promedio de presion. Enciende un led verde a modo indicativo.

Para implementar la aplicación, se requiere la conexión de dos dispositivos.
-El dispositivo de medicion de presion, el cual debe ser conectado a CH1.
-El display para mostrar maximo, minimo o promedio. Este se conecta a {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}.


