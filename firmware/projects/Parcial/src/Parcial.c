/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial.h"
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
//#include "delay.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"
#include "arm_math.h"
#include "arm_const_structs.h"
/*==================[macros and definitions]=================================*/

//Ancho de ventana
#define WINDOW_WIDTH 250

//Frecuencia de muestreo
#define SAMPLE_PER (1/250)*10^(-3)  // Periodo de muestreo en ms

/*==================[internal data definition]===============================*/

//Booleano utilizado como bandera para comenzar una nueva ventana
bool new_window = false;

//Cantidad de datos a tomar
int N = 250;

//Vector que contiene la señal
float32_t senial[WINDOW_WIDTH];

//Variable auxiliar que contiene el dato a guardar en el vector señal
uint16_t aux_senial;

//Contador asociado a la posición del vector señal
uint16_t contador_senial = 0;

bool b_tec1 = false;
bool b_tec2 = false;
bool b_tec3 = false;

serial_config puerto_serie;

/*==================[internal functions declaration]=========================*/

//Funciones de interrupciones de las teclas
void Tecla1();
void Tecla2();
void Tecla3();
void Tecla4();

/** @brief Función llamada por timer.
 * Activa conversión AD.
 */
void TempInt();

/** @brief Función llamada por interrupción conversor AD. Es la mencionada en la config del struct del conversor
 * Lee dato convertido
 */
void LecturaAD();

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit(); //Inicializacion del System Clock
	LedsInit(); //Inicializo los Leds
	gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5}; //Vector de gpio_t para Inicializar el display
	ITSE0803Init(pines); //Init del display

	//Fijamos configuración para inicializar conversor AD
	analog_input_config ad_config;
	ad_config.input = CH1;
	ad_config.mode = AINPUTS_SINGLE_READ;
	ad_config.pAnalogInput = LecturaAD;

	AnalogInputInit(&ad_config);

	// Configuracion del puerto serie

	puerto_serie.port = SERIAL_PORT_PC; //Que puerto se selecciona
	puerto_serie.baud_rate = 9600; //Baudios
	puerto_serie.pSerial = NO_INT; //Si no se pretende Recibir datos, se pone NO_INT

	UartInit(&puerto_serie); //Init de UART con la config de arriba

	//Estructura para inicializar y configurar timer
	timer_config temporizador;
	temporizador.period = SAMPLE_PER; //Período de muestreo
	temporizador.timer = TIMER_A; //Se usa SysTick
	temporizador.pFunc = TempInt;

	TimerInit(&temporizador); //Init del timer
	TimerStart(TIMER_A); // Star o comienzo del timer

	//Variables utilizadas para calculo de maximo, minimo y promedio.
	uint16_t max;
	uint16_t min;
	uint8_t i;
	uint8_t j;
	uint8_t k;
	uint16_t prom;
	uint16_t sum =0;

	//Booleanos para saber que mostrar en el display
	bool b_tec1 = false;
	bool b_tec2 = false;
	bool b_tec3 = false;

	//Init Switches, para tratarlos como interrupciones
	SwitchActivInt (SWITCH_1,Tecla1);
	SwitchActivInt (SWITCH_2,Tecla2);
	SwitchActivInt (SWITCH_3,Tecla3);

	while(1)
	{
		if(new_window)
		{
			LedsOffAll();

			// Obtención del valor maximo de la senial y envio a pc
			// Este calculo va fuera del if(b_tec1), ya que necesito siempre controlar la presion max para cumplir el punto e ("alarma" visual)
			max = senial[0];
			for(i=1 ; i<WINDOW_WIDTH ; i++)
			{
				if(senial[i]>max)
				{
					max = senial[i];
				}
			}

			if (b_tec1)
			{
				LedOn(LED_2); // Enciendo LED_2 ES ROJO
				ITSE0803DisplayValue(max);
				UartSendString(puerto_serie.port , UartItoa(max, 10)); // 10 es la base en la que paso max. En este caso es decimal
				UartSendString(puerto_serie.port , " mmHg (max)\r\n");
			}

			// Obtención del valor minimo de la senial y envio a pc
			if (b_tec2)
			{
				min = senial[0];
				for(j=1 ; j<WINDOW_WIDTH ; j++)
				{
					if(senial[j]<min)
					{
						min = senial[j];
					}
				}
				LedOn(LED_1); // Enciendo LED_1 ES AMARILLO
				ITSE0803DisplayValue(min);
				UartSendString(puerto_serie.port , UartItoa(min, 10)); // 10 es la base en la que paso min. En este caso es decimal
				UartSendString(puerto_serie.port , " mmHg (min)\r\n");
			}

			// Obtencion del promedio de la senial y envio a pc
			if (b_tec3)
			{
				prom =0;
				for(k=0 ; k<WINDOW_WIDTH ; k++)
				{
					sum = sum + senial[k];
				}
				prom = sum / N;
				sum =0;

				LedOn(LED_3); // Enciendo LED_3 ES VERDE
				ITSE0803DisplayValue(prom);
				UartSendString(puerto_serie.port , UartItoa(prom, 10)); // 10 es la base en la que paso prom. En este caso es decimal
				UartSendString(puerto_serie.port , " mmHg (prom)\r\n");
			}

			// Analisis del Maximo de la presion ("alarmas")

			if (max < 50)
			{
				LedOn(LED_RGB_G);// Prendo led verde del RGB
			}
			else if (max < 150 && max > 50)
			{
				LedOn(LED_RGB_B);// Prendo led azul del RGB
			}
			else if (max > 150)
			{
				LedOn(LED_RGB_R);// Prendo led rojo del RGB
			}
			else
				LedsOffAll();

			new_window = false;
		}
	}
	return 0;
}

// Función del temporizador
void TempInt()
{
	// Función que comienza a convertir el dato AD, una vez que termina de convertir, llama a la función LecturaAD()
	AnalogStartConvertion();
}

// Funcion llamada cuando el conversor termina la conversion
void LecturaAD()
{
	// Lee dato y lo guarda en variable auxiliar
	AnalogInputRead(CH1, &aux_senial);
	aux_senial = aux_senial * 60.60; // Escalo la señal convertida, a un valor de presion de entre 0 y 200mmHg

	// Asigna el valor de la variable auxiliar al componente del vector correspondiente, separando en parte real e imaginaria
	if(contador_senial<WINDOW_WIDTH)
	{
		senial[contador_senial] = aux_senial; //
		contador_senial++; // Aumenta el valor del contador en uno
	}
	// Cuando el contador llega al máximo, se reinicia
	else
	{
		contador_senial = 0;
		new_window = true;
	}
}

void Tecla1() // Muestra el valor maximo
{
	b_tec1 = true;
	b_tec2 = false;
	b_tec3 = false;
}
void Tecla2() // Muestra el minimo
{
	b_tec1 = false;
	b_tec2 = true;
	b_tec3 = false;
}
void Tecla3() // Muestra el promedio
{
	b_tec1 = false;
	b_tec2 = false;
	b_tec3 = true;
}

/*==================[end of file]============================================*/
