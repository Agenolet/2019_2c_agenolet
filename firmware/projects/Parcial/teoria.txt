Teoria de Alejandro Genolet:
1) El conversor de la figura es un conversor Analogico/Digital llamado de Aproximaciones Sucesivas.
El conversor dispone de un bloque de Sample and Hold en la entrada de la se�al, un comparador, un Registro de aproximaciones Sucesivas (SAR) el cual necesita de una se�al de clock, y de un conversor que pase la se�al digital a un valor analogico para volver a hacer la comparacion en el comparador.
El valor de la salida se va aproximando con cada iteracion al valor Vin de la entrada, hasta llegar a el valor mas proximo posible.
En este conversor, el valor que se toma en Vin, se va comparando con distintos valores generados en el SAR y convertidos en el DAC. El SAR primero empieza colocando en el bit mas significativo un 1 y en el resto cero, este valor se compara (luego de pasar por el DAC) y en el caso de ser menor que Vin se cambia el bit mas significativo a 0, de lo contrario se deja en 1.
Luego de la primera comparacion, se modifica el siguiente bit (el segundo m�s significativo), colocandolo en 1 (el resto sigue en 0), y luego del DAC vuelve a comparar con Vin. Si el valor es mayor que Vin, se deja el 1, de lo contrario se pone en 0. 
De esta manera va logrando hallar un valor binario que m�s se aproxime al valor de Vin el cual se quiere convertir de Analogico a Digital (cantidad finita de valores).
Se puede hacer la analogia de este metodo al de algunas balanzas, que se iba comparando con distintos valores de pezas, empezando por la mas pesada.
Es un metodo muy utilizado en la actualidad.

-->a)Tiempo de adquisicion, es el timepo necesario entre la toma del dato analogico, y la salida. Esta dado por la resistencia y el capacitor en el circuito Sample and Hold.
Este valor limita la frecuencia de muestreo. 

Tiempo de conversion, es el tiempo entre que el dato esta listo para ser recibido y el momento en que se termina de hacer la cuantificacion y la conversion (con todo lo que ello implica, que depender� del conversor utilizado).
Otra forma de definirlo seria que es el tiempo entre que se dispone del dato para convertir y el tiempo donde se realizo la conversion completa.

b)El circuito Sample and Hold cumple la funcion de retener el valor de entrada un momento ya que este puede variar mucho y se harian comparaciones erroneas y/o imprecisas.
Esta formado por un switch (que puede ser un mosfet), una resistencia y un capacitor, cuando el switch esta cerrado, se carga el capacitor, y cuando se abre, el capacitor vuelca el valor (se descarga) para poder hacer la comparacion teniendo dicho valor estable.



2) El SysTick es un contador decendente de 24 bits el cual esta implementado dentro de los microprosesadores de amd cortex-m. 
Posee las siguientes caracteristicas: 
-Para acceder a los registros del systick es necesario hacerlo desde el modo privilegiado
-En algunos fabricantes, el systick no dispone del clock de referencia del prosesador.
-En circuitos con sistemas operativos en tiempo real, el systick se dispone para controlar las tareas del mismo y no para ser usado en la aplicacion.
-Puede no contar cuando el sistema se encuentra en modo sleep
-Se detiene el conteo en el modo de depuramiento/debug

Dispone de 4 registros, los cuales se ven en el diagrama. Uno es el de calibracion, otro es el que almacena el valor del conteo actual, el de Control y Estado el cual hace que comienze o se detenga el conteo y el de Reload Value que resetea el contador.







