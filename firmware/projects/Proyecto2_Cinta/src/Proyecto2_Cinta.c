/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_Cinta.h"
#include "../inc/Proyecto2_Cinta.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "switch.h"
#include "DisplayITS_E0803.h"
#include "Tcrt5000.h"
#include "delay.h"
/*==================[macros and definitions]=================================*/

#define NO_KEY 0
/*==================[internal data definition]===============================*/

uint8_t contador = 0; 		//Cuenta los objetos
uint8_t contador_hold; 		//Para guardar el conteo hasta ese momento
bool state_aux = false; 	//Un bool auxiliar para no cometer errores de contar dos veces el mismo objeto/persona/otro
bool on_off = 1;			//El encendido y el apagado del conteo
bool display = false; 		//Para identificar cuando debo mostrar por display y cuando no
/*==================[internal functions declaration]=========================*/

void Tecla1(); //Interupccion de la tecla 1, prende/apaga
void Tecla2(); //Detiene la muestra del valor o la deja continuar (es decir mostrar el valor del conteo actual)
void Tecla3(); //Resetea el conteo
//void Tecla4();
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
    gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};
	ITSE0803Init(pines);
	Tcrt5000Init(T_COL0);

	//Activacion de las interruciones
	SwitchActivInt( SWITCH_1 , Tecla1 );
	SwitchActivInt( SWITCH_2 , Tecla2 );
	SwitchActivInt( SWITCH_3 , Tecla3 );
	//SwitchActivInt( SWITCH_4 , Tecla4 );

    while(1)
    {
    	if(on_off == 1)
    	{
    		if (Tcrt5000State() == true)
    		{
    			if(state_aux == false)
    				++contador;
    	    		state_aux = true;
    		}
    		DelayMs(100); //Delay para evitar las ocilaciones y mal conteos
    		if(Tcrt5000State() == false)
    			state_aux = false;	//solo cuando vuelva detectar un falso (no objeto) puede volver a sumar
    	}
    	if(display == false)
    	ITSE0803DisplayValue(contador);
    	display = false;
	}
	return 0;
}

/*==================[end of file]============================================*/
void Tecla1() //Interupccion de la tecla 1, prende/apaga
{
	on_off = !on_off;
}
void Tecla2() //Detiene la muestra del valor o la deja continuar (es decir mostrar el valor del conteo actual)
{
	display = !display;
}
void Tecla3() //Resetea el conteo
{
	contador = 0;
}
