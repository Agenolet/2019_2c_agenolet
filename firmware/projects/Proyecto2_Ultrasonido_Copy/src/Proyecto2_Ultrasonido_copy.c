/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_Ultrasonido_copy.h"       /* <= own header */
#include "../inc/Proyecto2_Ultrasonido_copy.h"

#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "hc_sr4.h"
#include "DisplayITS_E0803.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/

#define NO_KEY 0

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Inicializaciones
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	HcSr04Init(T_FIL2,T_FIL3);
	gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};
	ITSE0803Init(pines);

	//Definiciones de algunas variables
	uint8_t teclas = 0;
	int16_t distancia = 0; //Var Distancia donde se asigna la lectura del Dispositivo Ultrasonido (HcSr04Read)
	bool in_cm = false; // 0  si en cm y 1 si en in (pulgadas es in)
	bool on_off = true; //Apaga o prende, para modificar o no la variable Distancia (medir o no medir)
	bool display = false; //Activa o desactiva la muestra de distancia en el display

    while(1)
    {
    	teclas = SwitchesRead(); //No esta implementadas las teclas como interrupciones. En Proyecto2_Cinta si lo está

    	if(teclas != NO_KEY)
    	{
    		DelayMs(100);

    		if(SwitchesRead() == teclas)
    		{
    			switch(teclas)
    			{
    				case SWITCH_1:
    					on_off = !on_off;
    				break;
    				case SWITCH_2:
    					display = true;
    				break;
    				case SWITCH_3:
    					in_cm = false;
    				break;
    				case SWITCH_4:
    					in_cm = true;
    				break;
    			}
    		}
    	}
    	if(on_off == true)
    	{
    		if(in_cm == false)
    			distancia = HcSr04ReadDistanceCentimeters();
    		if(in_cm == true)
    			distancia = HcSr04ReadDistanceInches();
    	}

    	if(on_off == false)
    		distancia = 0;

    	if(display == false)
    	ITSE0803DisplayValue(distancia);

    	display = false;

    	DelayMs(500);
	}
	return 0;
}

/*==================[end of file]============================================*/

