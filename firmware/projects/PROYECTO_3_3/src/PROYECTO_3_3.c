/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


/* Esta aplicacion lo que realiza es lo mismo que el proyecto 2 pero utilizando TIMERS*/




/*==================[inclusions]=============================================*/
#include "../../PROYECTO_3_2/inc/PROYECTO_3_2.h"       /* <= own header */

#include "switch.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "bool.h"
#include "delay.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "string.h"

/*==================[macros and definitions]=================================*/
#define RETARDO 100
#define BLANCO 0
#define ANTI_REBOTE 100
#define VEL_TRANSF 115200
#define VEL_TRANS_BLUETOOTH 9600
#define DECIMAL 10




/*==================[internal data definition]===============================*/
	bool estado_T1=false;												//Bandera que verifica el estado de la tecla 1
	bool unidad=true;													//TRUE= CM ........ FALSE=INCHES
	bool hold=false;													// Bandera que verifica el estado de la tecla 2
	bool ref_act=true;													// Bandera que activa el refresco cada 1 segundo

	/*==================[internal functions declaration]=========================*/

		//INTERRUPCIONES DEL SWITCHES


	//CAMBIA EL ESTADO DE MI BANDERA HOLD
	void Hold(void){
		hold=!hold;
	}
	//CAMBIA EL ESTADO DE MI BANDERA A CM
	void UnitCm(void){
		unidad=true;
	}
	//CAMBIA EL ESTADO DE MI BANDERA A PULGADAS
	void UnitInches(void){
		unidad=false;
	}

	//INTERRUPCIONES DEL TIMER
	void Refresco(void){
		ref_act=!ref_act;

	}



/*==================[external data definition]===============================*/

	serial_config ComunicationChannel_1={SERIAL_PORT_RS485,VEL_TRANSF,NO_INT}; //INICIALIZACION DEL PUERTO DE COMUNICACION SERIE
	serial_config ComunicationChannel_2={SERIAL_PORT_P2_CONNECTOR,VEL_TRANS_BLUETOOTH,NO_INT};

	timer_config refresh={TIMER_B,1000,Refresco};						//Definimos el TIMER de 1 segundo, el cual debe de ejecutar refresco

/*==================[external functions definition]==========================*/

	//CAMBIA EL ESTADO DE MI BANDERA TECLA 1, A SU VEZ INICIALIZA AL TIMER
	void MedicionAct(void){

		estado_T1=!estado_T1;
		if(estado_T1==true){
			TimerStart(refresh.timer);
		}
		else{
			TimerStop(refresh.timer);
		}
	}



int main(void){

	//INICIALIZACION DE LOS DRIVERS DE DISPOSITIVOS
	SystemClockInit();
	UartInit(&ComunicationChannel_2);
	HcSr04Init(T_FIL2,T_FIL3); 											//inicializamos el sensor con los pines correspondientes
	gpio_t pines[]={LCD1,LCD2,LCD3,LCD4,GPIO1,GPIO3,GPIO5};				//definimos los pines que utilizara mi display
	ITSE0803Init(pines);	 											//inicializamos los pines para la pantalla
	SwitchesInit();														//inicializamos los switches
	LedsInit();															//inicializamos switches y leds
	TimerInit(&refresh);												//inicializamos el timer

	//DECLARAMOS LAS VARIABLES A UTILIZAR
	uint16_t medida=0;													//distancia que censa mi sonar

	//INTERRUPCIONES
			//SWITCHES
	SwitchActivInt(SWITCH_1, MedicionAct);
	SwitchActivInt(SWITCH_2, Hold);
	SwitchActivInt(SWITCH_3, UnitCm);
	SwitchActivInt(SWITCH_4, UnitInches);

    while(1){

    	if(ref_act==true){												//Verifica que se deba realizar el refresco
    		if(hold==false){											//Verifica que no este activa la tecla HOLD
    			LedOff(LED_1);
    			LedOn(LED_RGB_B);										//Se prende en LED RGB, indicando la activacion de mi dispositivo
    			LedOn(LED_RGB_R);
    			LedOn(LED_RGB_G);
    			if(unidad==true){
    				LedOff(LED_3);										//apaga el LED que indica la medicion en pulgadas
    				medida=HcSr04ReadDistanceCentimeters();				//LEEMOS LA MEDIDA
    				UartSendString(ComunicationChannel_2.port,UartItoa(medida,DECIMAL));
    				UartSendString(ComunicationChannel_2.port," cm \r\n");
       				LedOn(LED_2);										//prendemos el LED indicador de cm
    				}
    			else{
    				LedOff(LED_2);										//apagamos el led indicador de cm
    				medida=HcSr04ReadDistanceInches();					//leemos la medida en pulgadas
    				UartSendString(ComunicationChannel_2.port,UartItoa(medida,DECIMAL));
    				UartSendString(ComunicationChannel_2.port," in \r\n");
    				LedOn(LED_3);										//prendemos el led indicador de pulgadas
    				}

    			ITSE0803DisplayValue(medida);
    			}//cierre del if(hold==false)
        	else{
        		LedOn(LED_1);
        		ITSE0803DisplayValue(medida);
        		}

    	ref_act=false;													//con esta linea nos aseguramos de que no se ejecute la medida en menos de 1 segundo
    	}//cierre del if(ref_act)

    	if(estado_T1==false){
			LedOff(LED_RGB_B);										//APAGAMOS EL DISPOSITIVO
			LedOff(LED_RGB_G);
			LedOff(LED_RGB_R);
			LedOff(LED_3);
			LedOff(LED_2);
			LedOff(LED_1);
			ITSE0803DisplayValue(BLANCO);
		}
    }//cierre del while


	return 0;
}

/*==================[end of file]============================================*/

