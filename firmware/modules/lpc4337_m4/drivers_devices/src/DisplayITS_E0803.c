/* @brief  EDU-CIAA NXP GPIO driver
 *
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

#include "DisplayITS_E0803.h"


/*****************************************************************************
 * Private macros/types/enumerations/variables definitions
 ****************************************************************************/

uint16_t aux;
gpio_t pines[7];


/*****************************************************************************
 * Public types/enumerations/variables declarations
 ****************************************************************************/

/*****************************************************************************
 * Private functions definitions
 ****************************************************************************/
void BinaryToBcd (uint16_t data, uint8_t digits, uint8_t * bcd_number );

/*****************************************************************************
 * Private functions declarations
 ****************************************************************************/

void BinaryToBcd (uint16_t data, uint8_t digits, uint8_t * bcd_number )
{
	int j;
	for(j = 0; j < digits; ++j)
	{
		bcd_number[j] = data % 10;
		data = data / 10;
	}
}
/*****************************************************************************
 * Public functions declarations
 ****************************************************************************/

bool ITSE0803Init(gpio_t * pins)
{
	uint8_t i;
	for(i=0; i<7; i++)
	{
	  GPIOInit(pins[i], GPIO_OUTPUT);
	  pines[i] = pins[i];
	}
	return (1);
}

bool ITSE0803DisplayValue(uint16_t valor)
{
	uint8_t arregloBCD[3];
	BinaryToBcd(valor, 3, arregloBCD);

	/* Mediante mascaras obtenemos los bits correspondientes*/
	uint8_t mascara = 1;

	int i,j;
	for(i=0; i<3; i++)
	{
		for(j=0; j<4; j++)
		{
			GPIOState(pines[j], arregloBCD[i] & (mascara<<j)); //A la unidad la separamos en bits
		}

		//Pulso de selección
		GPIOOn(pines[6-i]);
		GPIOOff(pines[6-i]);
	}
	return(1);
}

uint16_t ITSE0803ReadValue(void)
{
	return(aux);
}

bool ITSE0803Deinit(gpio_t * pins)
{
	GPIODeinit();
	return(1);
}

