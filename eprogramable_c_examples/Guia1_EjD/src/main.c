/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/

typedef struct
{
    uint8_t port;                /*!< GPIO port number */
    uint8_t pin;                /*!< GPIO pin number */
    uint8_t dir;                /*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

void FunA(uint8_t aux, gpioConf_t *vector );


/*==================[internal functions declaration]=========================*/
int main(void)
{
	uint8_t bcd;
	bcd=3;
	gpioConf_t vector1[] =
	{
	        { 1 , 4 , 1},
			{ 1 , 5 , 1},
			{ 1 , 6 , 1},
			{ 2 , 14 , 1}
	};
	FunA(bcd,vector1);

	return 0;
}

void FunA(uint8_t aux, gpioConf_t *vector )
{
	uint8_t mask[] =
	{
			1 , 1<<1 , 1<<2 , 1<<3
	};

	int i;
	for (i=0 ; i<4 ; i++)
	{
		if (aux & mask[i])
			printf("Prendo puerto %d . %d \n\r", vector[i].port , vector[i].pin);

	}
}

/*==================[end of file]============================================*/

