########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = Guia1_Ej1
#NOMBRE_EJECUTABLE = Guia1_Ej1.exe

#PROYECTO_ACTIVO = Guia1_Ej7
#NOMBRE_EJECUTABLE = Guia1_Ej7.exe

#PROYECTO_ACTIVO = Guia1_Ej9
#NOMBRE_EJECUTABLE = Guia1_Ej9.exe

#PROYECTO_ACTIVO = Guia1_Ej12
#NOMBRE_EJECUTABLE = Guia1_Ej12.exe

#PROYECTO_ACTIVO = Guia1_Ej14
#NOMBRE_EJECUTABLE = Guia1_Ej14.exe

#PROYECTO_ACTIVO = Guia1_Ej16
#NOMBRE_EJECUTABLE = Guia1_Ej16.exe

#PROYECTO_ACTIVO = Guia1_EjA
#NOMBRE_EJECUTABLE = Guia1_EjA.exe

#PROYECTO_ACTIVO = Guia1_EjB
#NOMBRE_EJECUTABLE = Guia1_EjB.exe

#PROYECTO_ACTIVO = Guia1_EjC
#NOMBRE_EJECUTABLE = Guia1_EjC.exe

PROYECTO_ACTIVO = Guia1_EjD
NOMBRE_EJECUTABLE = Guia1_EjD.exe





