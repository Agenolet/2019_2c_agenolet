/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Guia1_EjA/inc/main.h"

#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/

	typedef struct
	{
	    uint8_t n_led;        //indica el número de led a controlar
	    uint8_t n_ciclos;   //indica la cantidad de cilcos de encendido/apagado
	    uint8_t periodo;    //indica el tiempo de cada ciclo
	    uint8_t mode;       //ON, OFF, TOGGLE
	}led;

#define ON 1
#define OFF 0
#define TOGGLE 2



void FuncionA (led *aux)
{
	int i;
	int j;
	switch (aux->mode)
	{
	case ON:
		switch (aux->n_led)
		{
		case 1:
			printf("Enciende led1");
		break;
		case 2:
			printf("Enciende led2");
		break;
		case 3:
			printf("Enciende led3");
		break;
		}
	break;

	case OFF:
		switch (aux->n_led)
		{
		case 1:
			printf("Apaga led1");
		break;
		case 2:
			printf("Apaga led2");
		break;
		case 3:
			printf("Apaga led3");
		break;
		}
	break;

	case TOGGLE:

		for (i=0 ; i<(aux->n_ciclos) ; i++)
		{
			switch (aux->n_led)
			{
			case 1:
				printf("Toggle led1");
				break;
			case 2:
				printf("Toggle led2");
				break;
			case 3:
				printf("Toggle led3");
				break;
			}
			for (j=0 ; j<(aux->periodo) ; j++)
				printf("Retardo %d \n\r",j);
		}
	break;
	}
}



/*==================[internal functions declaration]=========================*/

int main(void)
{
    //Ej A

	led h;
	h.mode = TOGGLE;
	h.n_led = 3;
	h.n_ciclos = 10;
	h.periodo = 20;

	FuncionA(&h);

	return 0;
}

/*==================[end of file]============================================*/

